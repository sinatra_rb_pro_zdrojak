%w{rubygems sinatra json open-uri yaml fileutils}.each { |lib| require lib }

DEFAULT_TWEETER = 'zdrojak' unless defined? DEFAULT_TWEETER

class TweetReader

  attr_reader :results, :id

  FileUtils.mkdir_p('tmp') unless File.exist?('tmp') # Vytvorime adresar pro cache, pokud neexistuje

  def initialize(id)
    @id = id
  end

  def last_modified
    if time = File.mtime(cached_file)
      (time < Time.now-20) ? Time.now : time
    end
  end

  def load
    if cached? and not stale?
      load_cached
    else
      load_remote and cache!
    end
  end

  private

  def cached_file
    File.join('tmp', "cached_#{id}.yml")
  end

  def load_cached
    puts "* Loading cached data"
    @results = YAML.load_file( cached_file )
  end

  def load_remote
    puts "* Loading data from remote service"
    @results = JSON.parse( open("http://search.twitter.com/search.json?q=from%3A#{id}").read )['results'].collect do |r| 
      { :created_at => Time.parse(r['created_at']).strftime('%d/%m %H:%M'), :text => r['text'] }
    end
  end

  def cache!
    File.open(cached_file, 'w') { |file| YAML.dump(@results, file) }
  end

  def stale?
    File.mtime(cached_file) < Time.now-20 # Expirace po 20 sekundach
  end

  def cached?
    File.exist?(cached_file)
  end

end

get '/' do
  redirect( "/#{DEFAULT_TWEETER}" )
end

get '/:tweeter' do
  @tweeter = TweetReader.new( params[:tweeter] || DEFAULT_TWEETER )
  if time = @tweeter.last_modified # 304 Not Modified, pokud se zdroj dat nezmenil
    last_modified( time )
  end
  @tweeter.load
  erb :index
end

use_in_file_templates!

__END__

@@ layout
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>JSON Client</title>
</head>
<body>
<h1>Novinky od <em><%= @tweeter.id %></em></h1>
<%= yield %>
</body>
</html>

@@ index
<ul>
<% @tweeter.results.each do |result| %>
  <li><em><%= result[:created_at] %>:</em> <%= result[:text] %></li>
<% end %>
</ul>
